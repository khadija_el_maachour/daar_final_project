import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookDataService {

  constructor(private http: HttpClient) { }
  private baseURL:string = "http://localhost:4200/"
  private getAllBooksURL:string = "getAllBooks"  // http://127.0.0.1:8000/getAllBooks
  private simpleSearchURL:string = "searchSimple"  // http://127.0.0.1:8000/searchSimple/<word>
  private advancedSearchURL:string = "advancedSearch"  // http://127.0.0.1:8000/advancedSearch/<word>

  getAllBooks(word:string): Observable<any> {
    const url = `${this.baseURL}${this.getAllBooksURL}`;
    console.log(" voici url ====  ",url)
    return this.http.get<Object>(url).pipe(
      tap (
          success => console.log('success response'),
          error => console.log('error')
       ), 
    );
	}
  
  
  simpleSearchBooks(word:string): Observable<any> {
    const url = `${this.baseURL}${this.simpleSearchURL}/${word}`;
    console.log(" voici url ====  ",url)
    return this.http.get<Object>(url).pipe(
      tap (
        success => console.log('success response'),
        error => console.log('error')
        ), 
        );
      }

    advancedSearchBooks(word:string): Observable<any> {
      const url = `${this.baseURL}${this.advancedSearchURL}/${word}`;
      console.log(" voici url ====  ",url)
      return this.http.get<Object>(url).pipe(
        tap (
            success => console.log('success response'),
            error => console.log('error')
          ), 
      );
    }
}

