import { Component, OnInit, Input } from '@angular/core';
import { BookModel } from 'src/app/models/bookModel';
import{BookDataService} from 'src/app/services/book-data.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  
  @Input() word:string = "";
  
  books : BookModel[] = [];
  suggestions : BookModel[] = [];
  asc:boolean = false;
  allBooks: BookModel[] = []
  

  constructor(private bookDataService:BookDataService) {}
  ngOnInit(): void {
    var d1 = new Date().getTime()
    // this.bookDataService.getAllBooks(this.word)
    //                            .subscribe(result => {
    //                               this.allBooks = result.data ;
    //                               console.log(result);
    //                               this.books=this.allBooks;
    //                               var d2 = new Date().getTime();
    //                               console.log(`time ${d2-d1}ms`);
    //                             }) 
    // this.changeOrder()
  }

  resetView(){
    this.books=this.allBooks;
    this.suggestions=[];
  }
  
  advancedSearch():void {
    var inputAdvancedSearch :HTMLInputElement = document.body.querySelector("input#isAdvancedSearch") as HTMLInputElement      
    inputAdvancedSearch.checked =  !inputAdvancedSearch.checked
  }

  changeOrder(){
    this.asc = !(this.asc);
    var spanOrder  = document.querySelector('.order') as HTMLElement
    if (spanOrder.innerText == "(Croissant)"){
      spanOrder.innerText="(Décroissant)";
    }else{
      spanOrder.innerText="(Croissant)";
    }

    if (this.asc){
      this.books.sort((a:BookModel,b:BookModel) => {
        return a.rank - b.rank;
      })
      this.suggestions.sort((a:BookModel,b:BookModel) => {
        return a.rank - b.rank;
      })
    }else {
      this.books.sort((a:BookModel,b:BookModel) => {
        return b.rank - a.rank;
      })
      this.suggestions.sort((a:BookModel,b:BookModel) => {
        return b.rank - a.rank;
      })
    }
  }

  onSubmit(): void {
    console.log("mot recherché : "+this.word); 
    if (!!this.word){
      var inputAdvancedSearch :HTMLInputElement = document.body.querySelector("input#isAdvancedSearch") as HTMLInputElement
      var d1 = new Date().getTime()    
      if (inputAdvancedSearch.checked){
        this.bookDataService.advancedSearchBooks(this.word)
                               .subscribe(result => {
                                  console.log(result.books)
                                  console.log(result.suggestions_books)
                                  this.books = result.books ;
                                  this.suggestions = result.suggestions_books ;
                                  var d2 = new Date().getTime();
                                  console.log(`time ${d2-d1}ms`);
                                })        
      }else {
        this.bookDataService.simpleSearchBooks(this.word)
                               .subscribe(result => {
                                  console.log(result.books)
                                  // console.log(result.suggestions_books)
                                  this.books = result.books ;
                                  // this.suggestions = result.suggestions_books ; 
                                  var d2 = new Date().getTime();
                                  console.log(`time ${d2-d1}ms`);
                                })      
      }
      if (this.asc){
        this.books.sort((a:BookModel,b:BookModel) => {
          return a.rank - b.rank;
        })
        this.suggestions.sort((a:BookModel,b:BookModel) => {
          return a.rank - b.rank;
        })
      }else {
        this.books.sort((a:BookModel,b:BookModel) => {
          return b.rank - a.rank;
        })
        this.suggestions.sort((a:BookModel,b:BookModel) => {
          return a.rank - b.rank;
        })
      }
    }else {
      this.resetView()
    }
  }


  submit(): void {
    this.onSubmit()
  }
}
