
export class BookModel {
    id            : number;
    title         : string;
    authors       : any;
    languages     : { languages: string[]};
    cover_url     : string
    content_url   : string;   
    rank          : number

    constructor(BookId?:number | any , title?:string | any, authors?: any , languages?:string | any ,content_url?:string | any, cover_url?:string| any, rank?:number|any){ 
        this.id          = BookId;
        this.title       = title;
        this.authors     = authors;
        this.languages   = languages ; 
        this.content_url = content_url ; 
        this.cover_url   = cover_url;
        this.rank        = rank ; 
    }

}