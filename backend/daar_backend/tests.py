from django.test import TestCase
from daar_backend.models import book_model, table_index_model, book_model_serializer,table_index_model_serializer, grapheJaccard_model, grapheJaccard_model_serializer
from daar_backend.graphe_Jaccard import GrapheJAccard


class Test(TestCase):
    list_lang =[]
    list_lang.append("english")
    languages = {'languages': list_lang}
    book1 = {
        "id": "5",
        "title":  "Server-side indexing and architectures",
        "author": "Binh-Minh Bui-Xuan",
        "languages": languages,
        "cover_url": "Not Found",
        "content_url": "https://www-apr.lip6.fr/~buixuan/files/daar2021/daar10.pdf"
    }

    book2 = {
        "id": "6",
        "title":  "Server-side indexing and architectures",
        "author": "EL MAACHOUR Khadija, Yalcin Erol",
        "languages": {'languages':["french"]} ,
        "cover_url": "Not Found",
        "content_url": "https://gitlab.com/khadija_el_maachour/daar_final_project/rapport.txt"
    }
    def test_addBookTest(self):
      book_serializer = book_model_serializer(data=self.book1)
      if (book_serializer.is_valid(raise_exception=True)):
          book_serializer.save()
      book = book_model.objects.filter(id="5")
      self.assertEqual(len(book), 1)

    def test_addGrapheTest(self):

        book_serializer1 = book_model_serializer(data=self.book1)
        book_serializer2 = book_model_serializer(data=self.book2)
        if(book_serializer1.is_valid()):
          book_serializer1.save()
        if(book_serializer2.is_valid()):
          book_serializer2.save()
        wordOccurence_book1 = {}
        wordOccurence_book = {}

        wordOccurence_book1["token1"]=3
        wordOccurence_book1["token2"]=2
        wordOccurence_book[self.book1["id"]]=wordOccurence_book1
        wordOccurence_book2 = {}
        wordOccurence_book2["token1"]=3
        wordOccurence_book2["token3"]=2
        wordOccurence_book[self.book2["id"]]=wordOccurence_book2

        graphe = GrapheJAccard()
        (graph_result, distanceOfAllBooks) = graphe.generate_grapheJaccard(wordOccurence_book)

        self.assertEqual(len(graph_result),2)
