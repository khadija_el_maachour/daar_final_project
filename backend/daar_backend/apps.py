from django.apps import AppConfig


class DaarBackendConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'daar_backend'
