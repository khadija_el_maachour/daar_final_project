
class GrapheJAccard:

    def __init__(self) -> None:
        self.SEUIL_DISTANCE = 0.75 

    # Methode permet de calculer la distance de jaccard entre deux livres
    def distance_jaccard(self,wordsBook1,woodsBook2):
        totalDifferenceOcc    = 0
        totalMax = 0
    
        wordsOcc = set(wordsBook1).intersection(woodsBook2)
        wordsOcc1 = list(set(wordsBook1).difference(woodsBook2))
        wordsOcc2 = list(set(woodsBook2).difference(wordsBook1))

        # words commune entre book1 et book2 
        for key in wordsOcc:
            k1 = wordsBook1[key]
            k2 = woodsBook2[key]
            totalMax +=  max(k1,k2) - min(k1,k2)
            totalDifferenceOcc    +=  max(k1,k2)

        # words existant seulement en book1
        for key in wordsOcc1:
            totalMax += wordsBook1[key]
            totalDifferenceOcc    += wordsBook1[key]

        # words existant seulement en book2
        for key in wordsOcc2:
            totalMax += woodsBook2[key]
            totalDifferenceOcc    += woodsBook2[key]
        try:
            return totalMax/totalDifferenceOcc
        except:
            return 1


    # Methode permet de generer le graphe de Jaccard 
    def generate_grapheJaccard(self, book_occurence_Words):

        graph_result = []
        distanceOfAllBooks = {}
        index = 1
        book_occurence_Words_list = list(book_occurence_Words)

        for book_id, wordsOcc in book_occurence_Words.items():
            for j in range(index, len(book_occurence_Words)):
                wordsOccBook1 = wordsOcc
                wordsOccBook2 = book_occurence_Words[book_occurence_Words_list[j]]
                dist_between_book1_book2 = self.distance_jaccard(wordsOccBook1, wordsOccBook2 )

                # Creer une arrete entre deux livre si la distance entre deux livre est infirieur au seuil
                if (dist_between_book1_book2 < self.SEUIL_DISTANCE):

                    new_elements = [ 
                                    { "node_src": book_id,"node_dist": book_occurence_Words_list[j]},
                                    { "node_src": book_occurence_Words_list[j], "node_dist": book_id}
                                ]
                    # Ajouter l'arrete          
                    graph_result.extend(new_elements)


                distanceOfAllBooks[book_id] = (distanceOfAllBooks[book_id]+dist_between_book1_book2) if (
                    book_id in distanceOfAllBooks.keys()) else dist_between_book1_book2

                distanceOfAllBooks[book_occurence_Words_list[j]] = (distanceOfAllBooks[book_occurence_Words_list[j]]+dist_between_book1_book2) if (
                    book_occurence_Words_list[j] in distanceOfAllBooks.keys()) else dist_between_book1_book2

            index += 1
        return (graph_result, distanceOfAllBooks)

