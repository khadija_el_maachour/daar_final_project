from lib2to3.pgen2 import token
from django.core.management.base import BaseCommand
from daar_backend.graphe_Jaccard import GrapheJAccard
from daar_backend.models import book_model, table_index_model, book_model_serializer,table_index_model_serializer, grapheJaccard_model, grapheJaccard_model_serializer
import requests
import html2text
import nltk
from nltk.tokenize import word_tokenize
from stop_words import get_stop_words
import re

class Command(BaseCommand):

   # retourne la liste de auteurs d'un livre
    def get_name_of_autors_book(self, book):
        list_autor =[]
        for autor in book['authors']:
            list_autor.append(autor['name'])

        return {'names':list_autor}

    # methode retourne la liste de auteurs d'un livre
    def get_book_languages(self, book):
        list_lang =[]
        for lang in book['languages']:
            list_lang.append(lang)

        return {'languages':list_lang}

    # Methode permet de supprimer les donnes stockées dans base de donnes
    def resetDatabase(self):
            book_model.objects.all().delete() #supprimer les livres
            table_index_model.objects.all().delete() #supprimer la table d'indexage
            grapheJaccard_model.objects.all().delete() # supprimer le graphe de jaccard

    # Methode permet de compter le nombre des mots dans un livre
    def get_number_of_word_book(self, book):
        try:
            content_url = book['formats']['text/plain; charset=utf-8'].replace(".zip", ".txt")
            content_text = requests.get(content_url).text
            h = html2text.HTML2Text()
            h.ignore_links = True
            textBook = h.handle(content_text)
            words = textBook.split()
            return len(words)
        except:
            return 0

    # Methode permet de retourner une paire de map de (word, occurence) et une liste des mots dans un livre) 
    def get_tokens(self, book):
        title = book['title']+" ".join(book['subjects'])
        tokens = re.split(r"[, !;%'?:}]", title.lower())
        # nltk.download()
        for lang in book['languages']:
            try:
                stop_words = get_stop_words(lang)
            except:
                stop_words = get_stop_words('en')

            tokens = [x for x in tokens if (x not in stop_words) and (len(x)>=3)]
        wordOccurence = {}
        for token in tokens:
            if (not(token in wordOccurence)):
                wordOccurence[token]=1
            else:
                wordOccurence[token]=wordOccurence[token]+1
        return wordOccurence, {"list_of_tokens": tokens}

    def handle(self, *args, **options):
        print("starting load data")
        api_url = "https://gutendex.com/" # lien de Api utilisé pour récuperer les données 
        number_of_books_min = 1664
        number_of_word_min = 10000    # nombre de mots minimum dans un livre 
        number_of_books = 0
        number_of_page = 1
        id_indexing = 0

        self.resetDatabase()
        booksWordsOccurence = {}
        books_words = []

        while(number_of_books < number_of_books_min ):
            data_from_api= requests.get(f'{api_url}/books?mime_type=text&page={number_of_page}')
            json_data = data_from_api.json()
            books = json_data['results']
            number_of_page = number_of_page + 1

            #parcourir les livres de chaque page   
            for book in books:
                if number_of_books > number_of_books_min:
                    break
                # accepter les livres qui ont au moins 10000 mots 
                if self.get_number_of_word_book(book) >= number_of_word_min:

                        names_of_autors = self.get_name_of_autors_book(book)
                        languages = self.get_book_languages(book)

                        try:
                            cover_url   = book['formats']['image/jpeg']
                            content_url = book['formats']["text/html"]      
                        except:
                            cover_url   = "Not defined"
                            content_url = "Not defined"
                        book_serializer = book_model_serializer(data = { 
                                                                        "id"         : book['id'],
                                                                        "title"      : book['title'], 
                                                                        "authors"    : names_of_autors,
                                                                        "languages"  : languages,
                                                                        "cover_url"  : cover_url,
                                                                        "content_url": content_url })

                        (wordOccurence, tokens) = self.get_tokens(book)
                        if book_serializer.is_valid(raise_exception=True):
                                book_serializer.save()
                                number_of_books += 1
                                booksWordsOccurence[book['id']]=wordOccurence

                                print(" Number of books stored  :  " , number_of_books)

                        else:
                                print("ERROR: book not stored")
                        
                        # Ajouter les tokens d'un lives dans la table d'indexage
                        books_words.append((book['id'],wordOccurence))
 
        # Construction de la table d'indexage inversé
        books_words_inversed = {}
        for b in books_words:
            id,wo = b
            for w, o in wo.items():
                if (not(w in books_words_inversed)):
                    books_words_inversed[w]=[id]
                else:
                    if (not (o in books_words_inversed[w])):
                        books_words_inversed[w]=books_words_inversed[w] + [id]
        
        for  w, ids in books_words_inversed.items():
            table_indexing_serializer = table_index_model_serializer(data = { 
                                                                "id"         : w,
                                                                "ref"        : ids
                                                                })
            
            if table_indexing_serializer.is_valid(raise_exception=True):
                    table_indexing_serializer.save()
            else:
                    print("ERROR: book index not stored ")

        # Generer le graphe de Jaccard
        graphe = GrapheJAccard()
        (graph_result, distanceOfAllBooks) = graphe.generate_grapheJaccard(booksWordsOccurence)
        serializerGraphJaccard = grapheJaccard_model_serializer(data=graph_result, many=True)
        print("lengh of Graphe =  ",len(graph_result))

        if serializerGraphJaccard.is_valid(raise_exception=True):
            serializerGraphJaccard.save()

         # Ajouter le Classement Crank des sommets par centralité de proximité
        for idBook in distanceOfAllBooks:
        # crank(v) = n − 1 / SUM (d(u, v))
            sum_of_distances = distanceOfAllBooks[idBook]
            number_of_neighbours  = len(distanceOfAllBooks) - 1
            crank_book = number_of_neighbours / sum_of_distances
            book = book_model.objects.get(id=idBook)
            book.rank = crank_book
            book.save()
            
        print("Number of books stored  :  " , number_of_books)

