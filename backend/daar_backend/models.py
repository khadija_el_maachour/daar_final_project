# from importlib.resources import contents
from django.db import models
from rest_framework.serializers import ModelSerializer
from django.contrib.postgres.fields import ArrayField


# le model d'un  livre 
class book_model(models.Model):
  id           = models.IntegerField(primary_key=True)
  title        = models.CharField(max_length=1000)
  authors      = models.JSONField(default=dict)
  languages    = models.JSONField(default=dict)
  cover_url    = models.CharField(max_length=800, default= "not defined" )
  content_url  = models.CharField(max_length=800 , default= "not defined")
  rank         = models.FloatField(default="0.0")

  class Meta:
        constraints = [
            models.UniqueConstraint(fields=['id','title'], name='unique')
        ]
        ordering = ['id' , 'title']

# Serialization d'un livre 
class book_model_serializer(ModelSerializer):
    class Meta:
        model = book_model
        fields = ('id',
                 'title',
                 'authors' ,
                 'languages', 
                 'cover_url', 
                 'content_url',
                 'rank'   
        )

# model de table d'indexage
class table_index_model(models.Model):
  id           = models.CharField(primary_key=True, max_length=100)
  ref          = models.JSONField()
 
# Serialization de table d'indexage
class table_index_model_serializer(ModelSerializer):
    class Meta:
        model = table_index_model
        fields = ('id',
                 'ref',
        )

# Model de graphe de Jaccard 
class grapheJaccard_model(models.Model):
  # Identifiant d'une arrete de graphe
  id           = models.AutoField(primary_key=True) 
  # le sommet Source 
  node_src     = models.IntegerField() 
   # le sommet Destination 
  node_dist    = models.IntegerField()

# Serialization de graphe de Jaccard 
class grapheJaccard_model_serializer(ModelSerializer):
    class Meta:
        model = grapheJaccard_model
        fields = ('id',
                 'node_src',
                 'node_dist'   
        )
