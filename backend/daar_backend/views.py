from click import command
from django.shortcuts import render

from daar_backend.models import book_model, table_index_model, book_model_serializer, table_index_model_serializer, grapheJaccard_model, grapheJaccard_model_serializer
from rest_framework.views import APIView
from django.http import HttpResponse
import json
import subprocess
import html2text
import requests
from collections import Counter
from stop_words import get_stop_words
import re
import csv

class RedirectionBooksList(APIView):
    def get(self, request, format=None):
        books = book_model.objects.all()
        jsondata = book_model_serializer(books, many=True)
        objectdata = {}
        objectdata['data']    = jsondata.data
        jsonString            = json.dumps(objectdata)
        return HttpResponse(jsonString, content_type="application/json", status=200, reason="get all books")


class GetAllGraphe(APIView):
    def get(self, request, format=None):
        graphe = grapheJaccard_model.objects.all()
        jsondata = grapheJaccard_model_serializer(graphe, many=True)
        objectdata = {}
        objectdata['data'] = jsondata.data
        jsonString = json.dumps(objectdata)
        return HttpResponse(jsonString, content_type="application/json", status=200, reason="get all graphe")

class RedirectionBooksIndexingList(APIView):
    def get(self, request, format=None):
        indexing_table = table_index_model.objects.all()
        jsondata = table_index_model_serializer(indexing_table, many=True)
        objectdata = {}
        objectdata['data'] = jsondata.data
        jsonString = json.dumps(objectdata)
        return HttpResponse(jsonString, content_type="application/json", status=200, reason="get table index")

class GetSimpleSearch(APIView):

    def get(self, request, word, format=None):
        print("Starting simple search")
        result = []
        books_result = []
        booksIndex = table_index_model.objects.all()
        for indexBook in booksIndex:
                if str.lower(indexBook.id) == str.lower(word):
                        print(indexBook.ref     )
                        book = book_model.objects.filter(id__in=indexBook.ref)
                        print(book)
                        books_result= book
                        break

        jsondata = book_model_serializer(books_result, many=True)
        objectdata = {}
        objectdata['books'] = jsondata.data
        jsonString = json.dumps(objectdata)
        return HttpResponse(jsonString, content_type="application/json", status=200, reason="get all books")


class GetSearchAdvanced(APIView):

    # Methode permet de compter le nombre de mot trouver par Egrep regEx dans un livre
    def getRegExCount(self, content, regEx:str):

        command = ['java', '-jar', '../egrep.jar', regEx, content]
        p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        count = 0
        for _ in iter(p.stdout.readline, b'') :
                count += 1

        return count

    # Methode qui verifie si une expression est regulier
    def isExpressionReg(self, exp):
        for i in range(len(exp)):
            if (((exp[i] == '*') |(exp[i] == '.') | (exp[i] == '|')) ):
                return True
        return False

    def get(self, request, word, format=None):
        print("Starting advanced search")
        listOfWords = word.split()
        listOfWords = [each_string.lower() for each_string in listOfWords]
        books_result = []
        book_occurence_word ={}
        useEgrepSearch = self.isExpressionReg(word)
        
        if useEgrepSearch :
            books = book_model.objects.all() 
            for book in books:
                occurence_count = self.getRegExCount(book.title, word)
                if occurence_count > 0:
                   b = book_model.objects.filter(id=book.id)
                   books_result += b
                   book_occurence_word[book.id] = occurence_count

        else:
            booksIndex = table_index_model.objects.all()  
            for indexBook in booksIndex:

                if str.lower(indexBook.id) in listOfWords:
                        book = book_model.objects.filter(id__in=indexBook.ref)
                        books_result += book

        for book in books_result:
            occurence_count = self.getRegExCount(book.title, word)
            book_occurence_word[book.id] = occurence_count

        # construire les suggestions
        relevant_books = dict(sorted(book_occurence_word.items(), key=lambda item: item[1],reverse=True))
        #recuperer les trois livre les plus pertinents 
        relevant_books_list = list(relevant_books)[:3]

        graphe_books_ids = grapheJaccard_model.objects.filter(node_src__in=relevant_books_list).exclude(node_dist__in=relevant_books_list)
        suggestions_books = []
        #Recuperer les voisins des livres les plus pertinents 
        for graphe in graphe_books_ids:
            suggestions_books+= book_model.objects.filter(id=graphe.node_dist)

        jsondata = book_model_serializer(books_result, many=True)
        suggestions_jsondata = book_model_serializer(suggestions_books, many=True)
        objectdata = {}
       
        objectdata['books'] = jsondata.data
        objectdata['suggestions_books'] = suggestions_jsondata.data
        jsonString = json.dumps(objectdata)
        return HttpResponse(jsonString, content_type="application/json", status=200, reason="get all books in database")


