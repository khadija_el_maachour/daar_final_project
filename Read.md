# Pour lancer le backend :

  - python -m venv env
  - source env/bin/activate
  - cd backend
  - pip3 install django djangorestframework psycopg2 click html2text requests strop_words nltk
  * décommenter la ligne  53 ( # nltk.download()) fichier getData.py pour télécharger les stopword pour la premiere fois
  - python manage.py makemigrations
  - python manage.py migrate
  * python manage.py getdata (cette action permet de (re)construire la base de données)

  - python3 manage.py runserver

# Pour lancer le front :
  - cd frontend/daar-frontend 
  - npm install
  - npm start 
